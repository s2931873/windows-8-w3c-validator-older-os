﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=392286
(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }
            args.setPromise(WinJS.UI.processAll());

            //set event handler for url form
            var validateButton = document.getElementById("validator");
            validateButton.addEventListener("click", buttonClickHandler, false);
            //hide loader on start
            $("progress").hide();
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. You might use the
        // WinJS.Application.sessionState object, which is automatically
        // saved and restored across suspension. If you need to complete an
        // asynchronous operation before your application is suspended, call
        // args.setPromise().
    };

    app.start();

    //method:: by matthew de marillac
    //validates a url's HTML doc from W3C JSON API
    //uses JQuery
    function buttonClickHandler(eventInfo) {
        try {
            //hide inputs to prevent double call to this method
            $("#uri, #validator, label").hide();
            //clear last action 
            $("#response").empty();
            //set ajax cache to false - alows user to valdiate page multiple times
            $.ajaxSetup({ cache: false });
            //get user url input
            var selectedURL = $("#uri").val();
            //API url
            var url = "http://validator.w3.org/check?output=json&uri=" + selectedURL;
            //show loading
            $("progress").show();
            //get JSON from API
            var api = $.getJSON(url,
                function (data) {
                    if (data != undefined) {
                        //header output
                        //if validation errors - show message and errors
                        if (data.messages.length > 0) {
                            $("#response").append("<h3 style='color:#ff0000;'>Validation Output: " +
                                window.toStaticHTML(data.messages.length +
                                " messages" + "</h3><br/>"));
                        } else {
                            //document is valid HTML- display success message and exit
                            $("#response").append("<h3 style='color:#00ff00;'>Document is valid HTML</h3>");
                            $("progress").hide();
                            $("#uri, #validator, label").show();
                            return;
                        }
                        //foreach error or warning print html reponse
                        $.each(data.messages, function (i, item) {
                            if (data.messages != undefined) {
                                //show error type icons
                                if (item.type == "error") {
                                    $("#response").append("<img class='icon' src='error.png'>");
                                } else {
                                    $("#response").append("<img class='icon' src='warning.png'>");
                                }
                                //show line and column error appears
                                if (item.type == "error") {
                                    $("#response").append(window.toStaticHTML("<i>Line " + item.lastLine +
                                        ", Column " + item.lastColumn + ": </i>"));
                                }
                                //print error message
                                $("#response").append(window.toStaticHTML(item.message + "<br/><br/>"));
                            }
                        })
                    }
                    //end loading
                    $("progress").hide();
                    $("#uri, #validator, label").show();
                    return;
                    //if ajax failed
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    //end loading and display error
                    $("#response").append("There was an error in connecting to the page!<br/>" +
                        "It could be down or an incorect address?" + "<!--" + errorThrown + "-->");
                    $("progress").hide();
                    $("#uri, #validator, label").show();
                    return;
                });
        } catch (exception) {
            $("#response").append("Sorry there was an unexpected error! Details:<br/>" + exception);
            $("progress").hide();
            $("#uri, #validator, label").show();
            return;
        }
    }

})();